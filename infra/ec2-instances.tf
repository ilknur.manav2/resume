resource "aws_instance" "kubernetes" {
  count         = 3
  ami           = "ami-08a0d1e16fc3f61ea"  # Amazon Linux 2 AMI ID for your region, you may need to update this
  instance_type = "t3.medium"
  key_name      = "k8s"  # Replace with your SSH keypair name
  
  vpc_security_group_ids = [aws_security_group.baseline_sg.id] 
  
  tags = {
    Name = "${element(["kubernetes-master", "worker1", "worker2"], count.index)}"
  }
}

resource "aws_instance" "ansible" {
  count         = 1
  ami           = "ami-08a0d1e16fc3f61ea"  # Amazon Linux 2 AMI ID for your region, you may need to update this
  instance_type = "t2.micro"
  key_name      = "k8s"  # Replace with your SSH keypair name

  vpc_security_group_ids = [aws_security_group.baseline_sg.id]

  tags = {
    Name = "ansible-controller"
  }
}
output "kubernetes_instance_ips" {
  value = [for instance in aws_instance.kubernetes : instance.public_ip]
}

output "ansible_instance_ip" {
  value = aws_instance.ansible[0].public_ip  # Assuming there's only one instance in the ansible resource
}
