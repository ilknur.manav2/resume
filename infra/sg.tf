# Define a security group
resource "aws_security_group" "baseline_sg" {
  name        = "baseline-security-group"
  description = "Baseline security group allowing SSH, HTTP, and HTTPS"

  # Ingress rules (incoming traffic)
  ingress {
    description = "Allow SSH from anywhere using SSH key pair"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # Allow SSH only from instances with the specified SSH key pair
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow HTTP from anywhere"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow HTTP from anywhere (open to all IP addresses)
  }

  ingress {
    description = "Allow HTTPS from anywhere"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow HTTPS from anywhere (open to all IP addresses)
  }

  # Egress rules (outgoing traffic)
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"  # All traffic
    cidr_blocks = ["0.0.0.0/0"]  # Allow all outbound traffic to anywhere
  }
}
